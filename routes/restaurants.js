var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");
var https = require('https');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

/*	Handle request send from client side at url: /restaurants
Send an external request to /cites? or /search? endpoints of Zomato APi
to get list of restaurants */
router.post('/', function(req, res, next) {
	let type = req.body.apiType;
	let searchCode = req.body.cityCodeRestaurant;
	var searchCode2 = req.body.cityCodePlace;
	let optionChecked = 2;
	let cityName = req.body.cityNameSearchBox;
	if (searchCode == 0) {
		res.render('error', {errorText: 'Sorry No Results Found.'});
	}

	let zomato = {
		city : "cities?",
		search : "search?",
		api_key : "acb43eed23c72f50f9d0273a5de65e3b"
	}

	let options = {
		hostname: 'developers.zomato.com',
		path : '/api/v2.1/', 
		method : 'GET',
		headers : {
			"user-key" : zomato.api_key
		}
	}

	/* Function to build ZomatoStr depends on the type
	Type1: trending restaurants
	Type2: restaurants around a specific coordinators */
	function buildZomatoStr(type) {
		let zomatoStr;
		if (type == 1) {
			let searchCode = req.body.cityCodeRestaurant;
			zomatoStr = zomato.search + 
					`entity_id=${searchCode}` +
					`&entity_type=city` +
					`&collection_id=1` +
					`&sort=rating`;
		} else {
			let lat = req.body.lat;
			let long = req.body.long;
			zomatoStr = zomato.search + 
					`&lat=${lat}` +
					`&lon=${long}` +
					`&radius=1000` +
					`&count=20` +
					`&sort=real_distance`;
		}
		return zomatoStr;
	}

	/* Function to parse results from response and 
	store only needed data in a new JSON object */
	function parseResults(restaurants) {
		let length = restaurants.length;
		let searchResults = `{"length": ${length}, "places" : [`;
		for (let i = 0; i < length; i ++) {
			let lat = restaurants[i].restaurant.location.latitude;
			let long = restaurants[i].restaurant.location.longitude;
			let lat2 = lat;
			let long2 = long;
			if (type == 2) {
				lat2 = req.body.lat;
				long2 = req.body.long;
			}
			let distance = calculateDistance(lat, long, lat2, long2);
			if (distance < 5000) {
				let resID = restaurants[i].restaurant.R.res_id;
				let url = restaurants[i].restaurant.url;
				let name = restaurants[i].restaurant.name;
				let address = restaurants[i].restaurant.location.address;
				let zipCode = restaurants[i].restaurant.location.zipcode;
				let cuisines = restaurants[i].restaurant.cuisines;
				let rating = restaurants[i].restaurant.user_rating.aggregate_rating;
				let votes = restaurants[i].restaurant.user_rating.votes;
				let imageUrl;
				if (restaurants[i].restaurant.featured_image) {
					imageUrl = restaurants[i].restaurant.featured_image;
				} else {
					imageUrl = "/images/foodNoImage.png";
				}
				let cost = restaurants[i].restaurant.average_cost_for_two;
				let currency = restaurants[i].restaurant.currency;
				let menuUrl = restaurants[i].restaurant.menu_url;
				searchResults += `{ "name" : "${name}" , `;
				searchResults += `"resID" : "${resID}" , `;
				searchResults += `"address" : "${address}" , `;
				searchResults += `"lat" : "${lat}" , `;
				searchResults += `"long" : "${long}" , `;
				searchResults += `"cuisines" : "${cuisines}" , `;
				searchResults += `"rating" : "${rating}" , `;
				searchResults += `"imageUrl" : "${imageUrl}" , `;
				searchResults += `"cost" : "${cost}" , `;
				searchResults += `"currency" : "${currency}" , `;
				searchResults += `"url" : "${url}" , `;
				searchResults += `"menuUrl" : "${menuUrl}" , `;
				if (i == (length - 1)) {
					searchResults += `"votes" : "${votes}" }`;
				} else {
					searchResults += `"votes" : "${votes}" },`;
				}
			}
			if (i == (length - 1)) {
				searchResults += " ]}";
			}
				
		}
		// preserve newlines, etc - use valid JSON
		searchResults = searchResults.replace(/\\n/g, "\\n")  
              	.replace(/\\'/g, "\\'")
              	.replace(/\\"/g, '\\"')
              	.replace(/\\&/g, "\\&")
              	.replace(/\\r/g, "\\r")
              	.replace(/\\t/g, "\\t")
              	.replace(/\\b/g, "\\b")
              	.replace(/\\f/g, "\\f");
		// remove non-printable and other non-valid JSON chars
		searchResults = searchResults.replace(/[\u0000-\u0019]+/g,""); 
		searchResults = JSON.parse(searchResults);
		searchResults.length = searchResults.places.length;
		if (searchResults.length == 0 ) {
			res.Status(500);
		}
		return searchResults;
	}

	/* Function to handle the response when the come back from the
	API endpoint */
	function handleResponse(body) {
		let bodyString = body.join('');
		let searchResults = JSON.parse(bodyString);
		searchResults = searchResults.restaurants;
		let length = searchResults.length;
		if (length > 0) {
			searchResults = parseResults(searchResults);
			if (type == 1) {
				res.render('searchResult', {searchResults, optionChecked, cityName, searchCode:searchCode2});
		}
			if (type == 2) {
				res.send({searchResults, length});
		}
		} else {
			res.render('error', {errorText: "Sorry no results found"});
		}
	}

	/* Function to calculate the distance between two points based on there coordinates */
	function calculateDistance(lat1, lon1, lat2, lon2) {
		var R = 6371e3; // metres
		var a1 = lat1 * Math.PI/180;
		var a2 = lat2 * Math.PI/180;
		var delta1 = (lat2-lat1) * Math.PI/180;
		var delta2 = (lon2-lon1) * Math.PI/180;

		var a = Math.sin(delta1/2) * Math.sin(delta1/2) +
        		Math.cos(a1) * Math.cos(a2) *
        		Math.sin(delta2/2) * Math.sin(delta2/2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

		var d = R * c;
		return d;
	}

	options.path += buildZomatoStr(type);

	/* Send external request to Zomato API to get list of 
	restaurants and handle the response */
	let zomatoReq = https.request(options, function(zomatoRes) {
		let body = []
		zomatoRes.on ('data', function(chunk) {
			body.push(chunk);
		})
		zomatoRes.on('end', function() {
			try {
				handleResponse(body);
			} catch(err) {
				next(err);
			}
		})
	})

	zomatoReq.on('error', (e) => {
		console.error(e);
		res.send(e.toString());
	});

	zomatoReq.end();
});

/*	Handle request send from client side at url: /restaurants/cityCode
Send an external request to /cites? endpoints of Zomato APi
to get cityCode of a city */
router.post('/cityCode', function(req, res, next) {
	var searchValue = req.body.search;
	searchValue = encodeURI(searchValue);
	let zomato = {
		city : "cities?",
		search : "search?",
		api_key : "acb43eed23c72f50f9d0273a5de65e3b"
	}
	let options = {
		hostname: 'developers.zomato.com',
		path : '/api/v2.1/', 
		method : 'GET',
		headers : {
			"user-key" : zomato.api_key
		}
	}

	let zomatoStr =	zomato.city + 
					`q=${searchValue}` +
					`&count=5`;

	options.path += zomatoStr;
	/* Function to handle the response when the come back from the
	API endpoint */
	function handleResponse(body) {
		let bodyString = body.join('');
		let results = JSON.parse(bodyString);
		results = results.location_suggestions[0].id;
		res.send(results.toString());		
	}

	/* Send external request to Zomato API to get cityCode of a city
	and handle the response */
	let cityCodeReq = https.request(options, function(cityCodeRes) {
			let body = []
			cityCodeRes.on ('data', function(chunk) {
				body.push(chunk);
			})

			cityCodeRes.on('end', function() {
				try {
					handleResponse(body);
				} catch (err) {
					next(err);
				}

			})
		})
	cityCodeReq.on('error', (e) => {
		console.error(e);
		res.send(e.toString());
	});

	cityCodeReq.end();
});

/*	Handle request send from client side at url: /restaurants/reviews
Send an external request to /reviews? endpoints of Zomato APi
to get the latest 5 reviews of a restaurant */
router.post('/reviews', function(req, res, next) {
	let restaurantID = req.body.resID;

	let zomato = {
		type : "reviews?",
		api_key : "acb43eed23c72f50f9d0273a5de65e3b"
	}

	let options = {
		hostname: 'developers.zomato.com',
		path : '/api/v2.1/', 
		method : 'GET',
		headers : {
			"user-key" : zomato.api_key
		}
	}

	let zomatoStr = zomato.type + 
					`res_id=${restaurantID}`;

	options.path += zomatoStr;

	/* Function to change any null fields into N/A */
	function validate(text) {
		if (!text) {
			return "N/A";
		}
		return text;
	}

	/* Function to parse results from response and 
	store only needed data in a new JSON object */
	function parseResults(reviews, length) {
		let searchResults = '{ "reviews" : [';
		for (let i = 0; i < length; i ++) {
			let rating = validate(reviews[i].review.rating);
			let reviewText = validate(reviews[i].review.review_text);
			let timestamp = validate(reviews[i].review.timestamp * 1000);
			let myDate = new Date(timestamp);
			timestamp = myDate.toString();
			timestamp = timestamp.substring(0,24);
			let name = validate(reviews[i].review.user.name);
			let foodieLevel = validate(reviews[i].review.user.foodie_level);
			let url = validate(reviews[i].review.user.profile_url);
			let imageUrl;
				
			reviewText = reviewText.replace(/"/g, '\\"');
			// remove non-printable and other non-valid JSON chars
			if (reviews[i].review.user.profile_image) {
				imageUrl = reviews[i].review.user.profile_image;
			} else {
				imageUrl = "/images/noAvatarIcon.png";
			}
			searchResults += `{ "name" : "${name}" , `;
			searchResults += `"rating" : "${rating}" , `;
			searchResults += `"reviewText" : "${reviewText}" , `;
			searchResults += `"timestamp" : "${timestamp}" , `;
			searchResults += `"foodieLevel" : "${foodieLevel}" , `;
			searchResults += `"url" : "${url}" , `;
			if (i == (length - 1)) {
				searchResults += `"imageUrl" : "${imageUrl}" } ]}`;
			} else {
				searchResults += `"imageUrl" : "${imageUrl}" },`;
			}			
		}
		
		// preserve newlines, etc - use valid JSON
		searchResults = searchResults.replace(/\\n/g, "\\n")  
              	.replace(/\\'/g, "\\'")
              	.replace(/\\"/g, '\\"')
              	.replace(/\\&/g, "\\&")
              	.replace(/\\r/g, "\\r")
              	.replace(/\\t/g, "\\t")
              	.replace(/\\b/g, "\\b")
              	.replace(/\\f/g, "\\f");
		// remove non-printable and other non-valid JSON chars
		searchResults = searchResults.replace(/[\u0000-\u0019]+/g,""); 
		
		searchResults = JSON.parse(searchResults);
		return searchResults;

	}

	/* Function to handle the response when the come back from the
	API endpoint */
	function handleResponse(body) {
		let bodyString = body.join('');
		let searchResults = JSON.parse(bodyString);
		let length = searchResults.reviews_shown;
		searchResults = searchResults.user_reviews;
		if (length > 0) {
			searchResults = parseResults(searchResults, length);
		} else {
			searchResults = "No Text Reviews Or No Review yet";
		}
		res.send({searchResults, length});
	}

	/* Send external request to Zomato API to get list of 
	reviews of a restaurant and handle the response */
	let zomatoReq = https.request(options, function(zomatoRes) {
			let body = []
			zomatoRes.on ('data', function(chunk) {
				body.push(chunk);
			})

			zomatoRes.on('end', function() {
				try {
					handleResponse(body);
				} catch(err) {
					next(err);
				}
				
			})
		})
		zomatoReq.on('error', (e) => {
			console.error(e);
			res.send(e.toString());
		});

		zomatoReq.end();
});
module.exports = router;
