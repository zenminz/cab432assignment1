var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");
var https = require('https');
var csvjson = require('csvtojson');
var csvFilePath = 'public/csv/cities.csv';

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());


/*	Handle request send from client side at url: /places
Send request to /places endpoint of Sygic Travel APi to get list of 
attraction places
*/
router.post('/', function(req, res, next) {
	//Get needed variables
	var searchCode = req.body.cityCodePlace;
	var optionChecked = 1; //Set optionChecked so client knows which to render
	let cityName = req.body.cityNameSearchBox;
	console.log(cityName);
	//Build request options
	let sygic = {
		type : "list?",
		api_key : "rbACyPqmUn9uNbv1bWqS03vsFR8yV7Ou3BRxqGNY"

	}
	let options = {
		hostname: 'api.sygictravelapi.com',
		path: "/1.0/en/places/",
		method: 'GET',
		headers: {
			"x-api-key": sygic.api_key
		}
	}
	let sygicStr = 	sygic.type +
				`parent=${searchCode}` +
				`&levels=poi` +
				`&limit=30` +
				`&categories=sightseeing|` +
				`playing|`+
				`discovering`;
	options.path += encodeURI(sygicStr);

	
	/* Function to parse results from response and 
	store only needed data in a new JSON object */
	function parseResults(places) {
		let length = places.length;

		let searchResults = `{ "length": ${length}, "places" : [`;
		for (let i = 0; i < length; i ++) {
			let id = places[i].id;
			let name = places[i].name;
			let lat = places[i].location.lat;
			let long = places[i].location.lng;
			let perex = places[i].perex;
			if (!perex) {
				perex = "N/A";
			} else {
				perex = perex.replace(/\"/g, "\\\"");
			}
			
			let thumbnail_url;
			if (places[i].thumbnail_url != null) { 
				thumbnail_url = places[i].thumbnail_url;
			} else {
				thumbnail_url = "/images/noImage.png";
			}
			searchResults += `{ "id" : "${id}" , `;
			searchResults += `"perex" : "${perex}" , `;
			searchResults += `"thumbnail_url" : "${thumbnail_url}" , `;
			searchResults += `"name" : "${name}" , `;
			searchResults += `"lat" : "${lat}" , `;
			if (i == (length - 1)) {
				searchResults += `"long" : "${long}" } ]}`;
			} else {
				searchResults += `"long" : "${long}" },`;
			}		
		}
		//Escape String and replace any weird characters
		searchResults = searchResults.replace(/\\n/g, "\\n")  
              	.replace(/\\'/g, "\\'")
              	.replace(/\\"/g, '\\"')
              	.replace(/\\&/g, "\\&")
              	.replace(/\\r/g, "\\r")
              	.replace(/\\t/g, "\\t")
              	.replace(/\\b/g, "\\b")
              	.replace(/\\f/g, "\\f");
		searchResults = searchResults.replace(/[\u0000-\u0019]+/g,""); 
		searchResults = JSON.parse(searchResults);
		return searchResults;
	}

	/* Function to handle the response when the come back from the
	API endpoint */
	function handleResponse(body) {
		let bodyString = body.join('');
		let searchResults = JSON.parse(bodyString);
		if (searchResults.status_code != 200) {
			res.render('error', {errorText : "API error. API key might be blocked."})
		} else {
			searchResults = searchResults.data.places;
			let searchResults2 = searchResults;	
			//Filter out any places which dont have any short discription (perex)
			searchResults = searchResults.filter(function(el) {
				return el.perex != null;
			});
			if (searchResults.length < 15) {
				searchResults = searchResults2;
				searchResults2 = null;	
			}	
			let length = searchResults.length;
			searchResults = parseResults(searchResults);
			res.render('searchResult', {searchResults, optionChecked, cityName, searchCode});	}	
	}
	/* Send external request to Sygic Travel API to get list of 
	attraction places and handle the response */
	let sygicReq = https.request(options, function(sygicRes) {
		let body = [];
		sygicRes.on ('data', function(chunk) {
			body.push(chunk);
		})
		sygicRes.on('end', function() {
			/*Render error page with the text no results found 
			when the information needed in the response is
			empty */
			try {
				handleResponse(body);
			} catch(err) {
				res.render('error', {errorText: "No Results Found"});
			}
		})
	})
	sygicReq.on('error', (e) => {
		console.error(e);
		res.send(e.toString());
	});

	sygicReq.end();
});

/*	Handle request send from client side at url: /places/placeDetail,
then send external request to /places/{id} endpoints of Sygic Travel APi
to get detailed information of a place
*/
router.post('/placeDetail', function(req, res, next) {
	//Get needed variables
	var placeID = req.body.placeID;
	//Build sygic api option to get the place's detail
	let sygic = {
		type : "list?",
		api_key : "rbACyPqmUn9uNbv1bWqS03vsFR8yV7Ou3BRxqGNY"

	}
	let options = {
		hostname: 'api.sygictravelapi.com',
		path: "/1.0/en/places/",
		method: 'GET',
		headers: {
			"x-api-key": sygic.api_key
		}
	}
	let sygicStr = placeID;
	options.path += sygicStr;

	/* Function to parse results from response and 
	store only needed data in a new JSON object */
	function parseResults(place) {
		let searchResults = '{ "place" : ';
		let name = place.name;
		let description;
		if (place.description != null) {
			description = place.description.text;
		} else {
			description = "No information";
		}
		description = description.replace(/\"/g, "\\\"");				
		let phone = place.phone;
		let address = place.address;
		let lat = place.location.lat;
		let long = place.location.lng;
		let rating = place.rating;
		let imageUrl;
		if (place.main_media != null) {
			imageUrl = place.main_media.media[0].url;
		} else {
			imageUrl = "/images/noImage.png";
		}

		searchResults += `{ "name" : "${name}" , `;
		searchResults += `"description" : "${description}" , `;
		searchResults += `"phone" : "${phone}" , `;
		searchResults += `"address" : "${address}" , `;
		searchResults += `"lat" : "${lat}" , `;
		searchResults += `"long" : "${long}" , `;
		searchResults += `"rating" : "${rating}" , `;
		searchResults += `"imageUrl" : "${imageUrl}" } }`;

		searchResults = searchResults.replace(/\\n/g, "\\n")  
              						.replace(/\\'/g, "\\'")
              						.replace(/\\"/g, '\\"')
              						.replace(/\\&/g, "\\&")
              						.replace(/\\r/g, "\\r")
              						.replace(/\\t/g, "\\t")
              						.replace(/\\b/g, "\\b")
              						.replace(/\\f/g, "\\f");
		searchResults = searchResults.replace(/[\u0000-\u0019]+/g,""); 
		searchResults = JSON.parse(searchResults);
		for (field in searchResults.place) {
			if (searchResults.place[field] == 'null') {
				searchResults.place[field] = "N/A";
			}
		}
		return searchResults;
	}

	/* Function to handle the response when the come back from the
	API endpoint */
	function handleResponse(body) {
		let bodyString = body.join('');
		let searchResults = JSON.parse(bodyString);
		if (searchResults.status_code != 200) {
			res.render('error', {errorText : "API error. API key might be blocked."})
		} else {
			searchResults = searchResults.data.place;
			searchResults = parseResults(searchResults);
			res.send(searchResults);
		}
		
	}

	/*Send external request to Sygic Travel API to get detail information
	of an attraction place and handle the response */
	let sygicReq = https.request(options, function(sygicRes) {
			let body = []
			sygicRes.on ('data', function(chunk) {
				body.push(chunk);
			})
			sygicRes.on('end', function() {
				try{
					handleResponse(body);
				} catch(err) {
					next(err);
				}				
			})
		})
	sygicReq.on('error', (e) => {
		console.error(e);
		res.send(e.toString());
	});

	sygicReq.end();
});

module.exports = router;
