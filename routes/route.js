var express = require('express');
var router = express.Router();
var bodyParser = require("body-parser");
var https = require('https');

router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

/* GET home page. */
router.post('/', function(req, res, next) {
	//Declare needed variables
	let placeLat = req.body.placeLat;
	let placeLong = req.body.placeLong;
	let lat = req.body.lat;
	let long = req.body.long;
	let mapbox = {
		type : "directions",
		routeType : {"driving" : "driving", "walking" : "walking"},
		api_key : "pk.eyJ1Ijoibjk2MjMzOTYiLCJhIjoiY2psZzl1dno4MGx4MjNwcDE1eXV4azhlOSJ9.9ChdjgoSi2BTpb0dgyE3Ag"
	}
	let options = {
		hostname: 'api.mapbox.com',
		path : `/${mapbox.type}/v5/mapbox/${mapbox.routeType.walking}/`, 
		method : 'GET',
	}
	let mapboxStr =	`${placeLong},` + 
					`${placeLat};` +
					`${long},` +
					`${lat}` +
					`?geometries=geojson` +
					`&access_token=${mapbox.api_key}`;

	options.path += mapboxStr;

	/* Function to Covert distance to a readable string */
	function convertM2KM(distance) {
		distance = Number(distance);
		let km = Math.floor(distance/1000);
		let m = Math.floor(distance%1000);
		let kmDisplay = km > 0 ? km + " km" : "";
		let mDisplay = m + " m";
		return kmDisplay + mDisplay;
	}

	/* Function to Convert duration to a readation string*/
	function convertS2H(time) {
		time = Number(time);
    	let h = Math.floor(time / 3600);
    	let m = Math.floor(time % 3600 / 60);
    	let s = Math.floor(time % 3600 % 60);
    	let hDisplay = h > 0 ? h + (h == 1 ? " hour " : " hours ") : "";
    	let mDisplay = m > 0 ? m + (m == 1 ? " minute " : " minutes ") : "";
    	let sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    	return hDisplay + mDisplay + sDisplay; 
	}

	/* Function to send data to the client side */
	function sendData(distance, duration, coordinates, type) {
		distance = convertM2KM(distance);		
		duration = convertS2H(duration)  + " of " + type;
		let length = coordinates.length;
		let searchResults = `{"distance" : "${distance}", "duration" : "${duration}", "coordinates" : [`;
		for (let i = 0; i < length; i++) {
			let lat = coordinates[i][1];
			let long = coordinates[i][0];
			searchResults += `[${lat}, `;
			if (i == (length - 1)) {
				searchResults += `${long}]]}`;
			} else {
				searchResults += `${long}], `;
			}
		}
		searchResults = JSON.parse(searchResults);
		res.send({searchResults, distance});
	}

	/* Function to get the walking route
	This function send external request to an mapbox direction API
	end point to retrieve the information back */
	function getWalkingRoute() {
		options.path = `/${mapbox.type}/v5/mapbox/${mapbox.routeType.driving}/`;
		options.path += mapboxStr;
		let walkingReq = https.request(options, function(walkingRes) {
			let body = [];
			walkingRes.on ('data', function(chunk) {
				body.push(chunk);
			})

			walkingRes.on('end', function() {
				let bodyString = body.join('');
				let results = JSON.parse(bodyString);
				let coordinates = results.routes[0].geometry.coordinates;
				let distance = results.routes[0].distance;
				let duration = results.routes[0].duration;
				sendData(distance, duration, coordinates, "driving");
			})
		})
		walkingReq.on('error', (e) => {
			console.error(e);
			res.send(e.toString());
		});

		walkingReq.end();
	}

	/* Send an external request to mapbox direction API endpoing
	to get the route from 2 points */ 
	let routeReq = https.request(options, function(routeRes) {
		let body = []
		routeRes.on ('data', function(chunk) {
			body.push(chunk);
		})

		routeRes.on('end', function() {
			let bodyString = body.join('');
			let results = JSON.parse(bodyString);
			try {
				let coordinates = results.routes[0].geometry.coordinates;
				let distance = results.routes[0].distance;
				let duration = results.routes[0].duration;
				if (distance > 800) {
					getWalkingRoute();
				} else {
					sendData(distance, duration, coordinates, "walking");
				}
			} catch(err) {
				next(err);
			}
		})
	})
	routeReq.on('error', (e) => {
		console.error(e);
		res.send(e.toString());
	});
	routeReq.end();
});


module.exports = router;
