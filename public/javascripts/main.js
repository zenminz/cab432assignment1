//Function to submit when submitBtN is clicked
	function submit() {
		$(".loader1").show();
		var selected = $('#optionList').children(":selected").val();
		if (selected == "restaurants") {
			var parameter = { search: $('#cityNameSearchBox').val()};
			$.ajax({
					type: 'POST', 
					data: parameter,
					url: '/restaurants/cityCode',
					success: function(result) {
						$('#cityCodeRestaurant').val(result);
						$('#searchBox').attr('action', "/" + selected);
						$('#apiType').val(1);
						$('#searchBox').submit();
						}
			}).fail(function() {			
				alert("No Results Found");
				$(".loader1").hide();
			})	
		} else {
			$('#searchBox').attr('action', "/" + selected);
			$('#searchBox').submit();
		}
	}
//end
$(document).ready(function() {
	$(".loader1").hide();
	$(window).keydown(function(event){
    	if(event.keyCode == 13) {
      		event.preventDefault();
    	}
  	});
	$('#submitBTN').click(submit)
});
	