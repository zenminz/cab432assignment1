
//Global variables declaration
	//Variable to store needed flags and values
		var placesList;
		var numPlace;
		var placeLat;
		var placeLong;
		var markerID = 0;
	//Create variable to store route
		var route;
	//Create global empty arrays to store places' marker and restaurants' marker 
		var arrayMarker = {};
		var arrayMarker2 = {};
	//Create variable to store a map
		var mymap;

//Global functions declerations
//These function are binded to elements in pug view files.
	//Function to open marker pop up when a result on Results Panel is clicked 
		window.openPopupResult = function (clicked_id, arrayMarkers) {
			markerID = clicked_id;
			let lat = arrayMarkers[markerID].getLatLng().lat;
			let long = arrayMarkers[markerID].getLatLng().lng;
			arrayMarkers[markerID].openPopup();
			mymap.setView([lat, long], 13);
		}
	//end

	//Function to highlight result on Results Panel when mouse hovers that result
		window.highLightResult2 = function(clickedId, color) {
			$(`#${clickedId}`).css("background-color", color);

		}
	//end

	//Function to get restaurant near the place
		window.findRestaurantOnClick = function() {
			$(".loader1").show();
			let lat = arrayMarker[markerID ].getLatLng().lat;
			let long = arrayMarker[markerID ].getLatLng().lng;
			placeLat = lat;
			placeLong = long;
			let parameters = {lat: lat, long: long, apiType : 2};
			$.ajax({
					type: 'POST',
					data: parameters,
					url: '/restaurants'
			}).done(function(results) {
				$('#findRestaurant').prop('disabled', true);
				deleteOldMarker(markerID);
				let placesList = results.searchResults.places;
				let numRes = results.length;
				createMarkerZomato(placesList, numRes, (markerID + 1));
				deleteOldDiv(markerID);
				buildResultBarRestaurant(placesList, numRes, (markerID + 1));
				mymap.setView([lat, long], 15);
				arrayMarker[markerID].getPopup().closePopup();
				$(".loader1").hide();
			}).fail(function() {
				alert("No Results Found.");
				$(".loader1").hide();
			})
		}
	//end

	//Function to get the route from the restaurant to the place
		window.findRoute = function() {
			$(".loader1").show();
			//$(".dimmer").show();
			let lat = arrayMarker2[markerID].getLatLng().lat;
			let long = arrayMarker2[markerID].getLatLng().lng;
			let parameters = {placeLong : placeLong, placeLat : placeLat, long : long, lat : lat};
			$.ajax({
				type: 'Post',
				data: parameters,
				url: '/route'
			})
			.done(function(results) {
				showRoute(results)
			})
			.fail(function(results) {
				alert("Sorry no route was found");
				$(".loader1").hide();
			});
		}
	//end

//Functions declarations
	//Function to create map
		function createMap(places) {
			let mapLat;
			let mapLong;
			if (!places) {
				mapLat = -27.4698;
				mapLong = 153.0251;
			} else {
				mapLat = places[0].lat;
				mapLong = places[0].long;
			}
			mymap = L.map('mapid').setView([mapLat, mapLong], 11);
			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
				attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom: 18,
				id: 'mapbox.streets',
				accessToken: 'pk.eyJ1Ijoibjk2MjMzOTYiLCJhIjoiY2psZzl1dno4MGx4MjNwcDE1eXV4azhlOSJ9.9ChdjgoSi2BTpb0dgyE3Ag'
				}).addTo(mymap);
			$(".loader1").hide();
		}
	//end

	//Function to create markers for attraction places option
		function createMarkerPlace(places, numPlaces) {
			for (let i = 1; i < numPlaces + 1; i ++) {
			let id = places[i - 1].id;	
			let lat = places[i - 1].lat;
			let long = places[i - 1].long;
			let popupStr = `<div class="ui active inverted dimmer">
    								<div class="ui text loader">Loading</div>
 							</div>`;
			popupStr += `<br><br><br><br><br><input type="hidden" id="place${i}" name="placeID" value="${id}">`;
			arrayMarker[i] = L.marker([lat, long], {myCustomId :  i})
								.addTo(mymap)
								.on('popupopen', changePopupContentSygic)
								.on('mouseover', highLightResult)
								.on('mouseout', undoHighLightResult);
			arrayMarker[i].bindPopup(popupStr);
			}
		}
	//end

	//Function to create markers for zomato option
		function createMarkerZomato(places, numRes, indexOffset) {
			try {
				let restaurantIcon = L.icon({
					iconUrl: '/images/restaurantIcon5.png',
					iconSize: [30, 35]
				});

				for (let i = 0; i < numRes; i ++) {
					let index = i + indexOffset;
					let resID = places[i].resID;
					let url = places[i].url;
					let menuUrl = places[i].menuUrl;
					let address = places[i].address;
					let lat = places[i].lat;
					let long = places[i].long;
					let name = places[i].name;
					let cuisines = places[i].cuisines;
					let rating = places[i].rating;
					let votes = places[i].votes;
					let imageUrl = places[i].imageUrl;
					let cost = places[i].cost;
					let currency = places[i].currency;
					let popupStr = `<div class="ui active inverted dimmer dimmerPopup">
    									<div class="ui text loader">Loading</div>
 									</div>`;
					popupStr += `<b>Detail</b><br>` +
									`<img src="${imageUrl}" width="300" height = "300" alt="Featured picture">` +
									`<br><a href="${url}" target="_blank"><h3>${name}</h3></a>` +
									`<b>Address:</b> ${address}` +
									`<br><b>Cuisines:</b> ${cuisines}` +
									`<br><b>Average Cost for 2 people:</b> ${currency} ${cost}` +
									`<br><a href="${menuUrl}" target="_blank"><b>Menu</b></a>` +
									`<br><b>Rating: ${rating}</b>` +
									` by <b>${votes}</b> people`+
									`<input type="hidden" id="resID${index}" name="resID" value="${resID}">` +
									`<div class="field" id="comments${resID}">`;
					arrayMarker2[index] = L.marker([lat, long], {myCustomId: index, icon: restaurantIcon})
										.addTo(mymap)
										.on('popupopen', changePopupContentZomato)
										.on('mouseover', highLightResult)
										.on('mouseout', undoHighLightResult);
					arrayMarker2[index].bindPopup(popupStr);			
				}
		} catch (err) {
			alert("No Results Found");
		}
	}
	//end

	//Function to create result panel for Zomato
		function buildResultBarRestaurant(places, numRes, indexOffset) {
			try {
				let str = `<div class="ui items">`;
				for (let i = 0; i < numRes; i ++) {
					let name = places[i].name;
					let url = places[i].imageUrl;
					let cuisines = places[i].cuisines;
					let rating = places[i].rating;
					let cost = places[i].cost;
					let currency = places[i].currency;
					let index = i + indexOffset;
					str +=		`<div class="item" id="${index}"
									onClick="openPopupResult(this.id, arrayMarker2)"
									onmouseover="highLightResult2(this.id, '#CCE2FF')"
									onmouseout="highLightResult2(this.id, '#ffffff')">`;
					str +=						`<div class="ui tiny image">`;
					str +=							`<img src="${url}" height="50" width="50">`;
					str +=						`</div>`;
					str +=				`<div class="content">`;
					str +=					`<a class="header">${name}</a>`;
					str +=						`<div class="meta">`;
					str +=							`<span>Description</span>`;
					str +=						`</div>`;
					str +=						`<div class="description">`;
					str +=							`<p>Cuisines: ${cuisines}</p>`;
					str +=							`<p>Cost per 2p.p.: ${currency} ${cost}</p>`;
					str +=						`</div>`;
					str +=						`<div class="extra">`;
					str +=							`Rating: ${rating}`;
					str +=						`</div>`;
					str +=					`</div>`;
					str +=			`</div>`;
				}
				$('#searchResults').append(str);
			} catch (err) {
				alert("No Results Found");
			}

		}
	//end

	//Function to create result panel for Sygic
		function buildResultBarPlace(places) {
			let str = `<div class="ui items">`;
			for (let i = 0; i < numPlace; i ++) {
				let name = places[i].name;
				let url = places[i].thumbnail_url;
				let description = places[i].perex;
				str +=		`<div class="item" id="${i+1}" 
							onClick="openPopupResult(this.id, arrayMarker)"
							onmouseover="highLightResult2(this.id, '#CCE2FF')"
							onmouseout="highLightResult2(this.id, '#ffffff')">`;
				str +=						`<div class="ui tiny image">`;
				str +=							`<img src="${url}" height="50" width="50">`;
				str +=						`</div>`;
				str +=				`<div class="content">`;
				str +=					`<a class="header">${name}</a>`;
				str +=						`<div class="meta">`;
				str +=							`<span>Description</span>`;
				str +=						`</div>`;
				str +=						`<div class="description">`;
				str +=							`<p>${description}</p>`;
				str +=						`</div>`;
				str +=						`<div class="extra">`;
				str +=							`Additional Details`;
				str +=						`</div>`;
				str +=					`</div>`;
				str +=			`</div>`;
			}
			$('#searchResults').append(str);
			$('#searchResults').append('</div>');
		}
	//end

	//Function to highlight result on Results Panel when a Marker is hover
		function highLightResult(event) {
			markerID = this.options.myCustomId;
			$(`#${markerID}`)[0].scrollIntoView({duration : "slow"});
			$(`#${markerID}`).css("background-color","#CCE2FF");
		}
	//end

	//Function to undo highlight
		function undoHighLightResult(event) {
			markerID = this.options.myCustomId;
			$(`#${markerID}`).css("background-color","#ffffff");
		}
	//end

	//Function to change popup content (Sygic)
		function changePopupContentSygic(event) {
			markerID = this.options.myCustomId;
			placeLat = arrayMarker[markerID ].getLatLng().lat;
			placeLong = arrayMarker[markerID].getLatLng().lng;
			var popup = this.getPopup();
			var placeID = $(`#place${markerID}`).val();
			var parameters = { placeID: placeID};
			var content;
			if ($(`#place${markerID}`).val()) {
				$.ajax( {
					type: 'POST',
					data: parameters,
					url: '/places/placeDetail'
				}).done(function(result) {
					let popupStr = `<b>Detail</b><br>` +
								`<img src="${result.place.imageUrl}" width="300" height = "300" alt="Featured picture">` +
								`<br><h3>${result.place.name}</h3>` +
								`<b>Address:</b> ${result.place.address}` +
								`<br><b>Phone:</b> ${result.place.phone}` +
								`<div class="descriptionWrapper">` +
								`<br><b>Description:</b> ${result.place.description}` +
								`</div>` +
								`<br><b>Rating: ${result.place.rating}</b>`+
								`<br><button onclick="findRestaurantOnClick()" class="ui button" id="findRestaurant" type="button">Find Restaurants</button>`;
					content = popupStr;
					popup.setContent(popupStr);
				}).fail(function() {
					alert("Looks like an error has occurred")
				})
			}
		}
	//end

	//Function to change popup content (Zomato)
		function changePopupContentZomato(event) {
			markerID = this.options.myCustomId;
			let resID = $(`#resID${markerID}`).val();
			let popup = this.getPopup();
			let content = popup.getContent();
			let parameters = { resID: resID };
			if ($(`#comments${resID}`).is(':empty')) {
				$.ajax( {
					type: 'POST',
					data: parameters,
					url: '/restaurants/reviews'
				}).done(function(result) {
					let numReviews = result.length;
					let popupStr = content;
					popupStr += `<br><br>`;
					popupStr += `<div class="field" id="comments">`;
					if (numReviews > 0) {
						popupStr += `<div class="ui items">`;
						for (let i = 0; i < numReviews; i ++) {
							let review = result.searchResults.reviews[i];
							let name = review.name;
							let rating = review.rating;
							let reviewText = review.reviewText;
							let timestamp = review.timestamp;
							let url = review.url;
							let imageUrl = review.imageUrl;
							let foodieLevel = review.foodieLevel;
							popupStr +=		`<div class="item">`;
							popupStr +=						`<div class="ui tiny image">`;
							popupStr +=							`<img src="${imageUrl}">`;
							popupStr +=						`</div>`;
							popupStr +=				`<div class="content">`;
							popupStr +=					`<a href="${url}" target="_blank" class="header">${name} (Level: ${foodieLevel})</a>`;
							popupStr +=						`<div class="meta">`;
							popupStr +=							`<span>Rated: ${rating}</span>`;
							popupStr +=						`</div>`;
							popupStr +=						`<div class="description">`;
							popupStr +=							`<p>${reviewText}</p>`;
							popupStr +=						`</div>`;
							popupStr +=						`<div class="extra">`;
							popupStr +=							`on ${timestamp}`;
							popupStr +=						`</div>`;
							popupStr +=					`</div>`;
							popupStr +=			`</div>`;
						}
						popupStr += `</div>`;					
					} else {
						popupStr += `<p>${result.searchResults}</p>`;
					}
					popupStr += `</div>`;
					popupStr += `<br><button onclick="findRoute()" class="ui button" id="findRoute" type="button">Find Route</button>`;
					popup.setContent(popupStr);
					$('.dimmerPopup').hide();
				}).fail(function() {
					alert("No detail can be found")
				})
			} else {
				$('.dimmerPopup').hide();
			}		
		}
	//end

	//Function to delete old markers when function findRestaurantOnClick is called
		function deleteOldMarker(id) {
			for (let i = 1; i < numPlace + 1; i ++) {
				let marker = arrayMarker[i];
				if (i != id) {
					mymap.removeLayer(marker);
				}
			}
		}
	//end

	//Function to delete old results on Results Panel when function findRestaurantOnClick is called
		function deleteOldDiv(id) {
			let divID = id;
			for (let i = 1; i < numPlace + 1; i++) {
				if (i != divID) {
					$(`#${i}`).remove();
					}
				}
		}
	//end


	//Function to display the route on the map
		function showRoute(results) {
			let coordinates = results.searchResults.coordinates;
			if (route) {
				mymap.removeLayer(route);
			}
			let distance = results.searchResults.distance;
			let duration = results.searchResults.duration;
			let popupStr = `<div class="field"><b>Travelling information</b></div>` +
							`<div class="field">This restaurant is <b>${distance}</b> away from the attraction place.</div>`	+
							`<div class="field">It will take approximately <b>${duration}</b> to get there.</div>`	
			route = L.polyline(coordinates, {color: 'blue'}).addTo(mymap).bindPopup(popupStr).openPopup();
			mymap.closePopup();
			route.getPopup().openOn(mymap);
			mymap.fitBounds(route.getBounds());
			$(".loader1").hide();
		}
	//end

//Execute functions when the document is ready
$(document).ready(function() {
	try {
		placesList = results;
		numPlace = length;
		createMap(placesList);
		$('#cityNameSearchBox').val(cityName);
		$('#cityCodePlace').val(cityCode);

		if (optionChecked == 1) {
			$('#optionList').val("places");
			createMarkerPlace(placesList, numPlace);
			buildResultBarPlace(placesList);
		} else {
			$('#optionList').val("restaurants");
			createMarkerZomato(placesList, numPlace, 0);
			buildResultBarRestaurant(placesList, numPlace, 0)	
		}
	} catch(err) {
		alert(err);
	}
	$(".loader1").hide();
})